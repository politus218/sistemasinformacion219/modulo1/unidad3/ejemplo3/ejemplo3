﻿-- Ejemplo 3 de procedimientos. Base de datos llamada ejemplo3.

-- Ejercicio 1. Importar los datos de Excel.

-- Tenemos todos los datos de las prácticas en un libro de Excel. Importar los 
-- datos y crear una tabla por cada hoja.
-- La tabla que tenga el mismo nombre que la hoja de cálculo que corresponda.
-- Primero se crea la base de datos desde el programa dbForge.
-- La importación y exportación así como la creación de las tablas se ha hecho
-- desde el programa de Excel.

-- Ejercicio 2. Realizar una función que reciba como argumentos:

-- Base de un triángulo
-- Altura de un triángulo
-- Debe devolver el cálculo del área del triángulo.

  DELIMITER //
  CREATE OR REPLACE FUNCTION AreaTriang(base int, altura int) 
  returns float
  BEGIN
    DECLARE area float;
    SET area=base*altura/2;  
    RETURN area;
  END //
  DELIMITER;

SELECT AreaTriang(5,6);

-- Ejercicio 3. Función perímetro triángulo

-- Realizar una función que reciba como argumentos:

-- Base de un triángulo
-- Lado2 de un triángulo
-- Lado3 de un triángulo
-- Debe devolver el cálculo del perímetro del triángulo.


DELIMITER //
CREATE OR REPLACE FUNCTION perimTrian(base int, lado2 int, lado3 int) 
returns float
BEGIN
  DECLARE perim float;
  SET perim=(base+lado2+lado3);
  RETURN perim;
END //
DELIMITER;

SELECT perimTrian(4,4,4);

-- Ejercicio 4. Procedimiento almacenado triángulos.

-- Realizar un procedimiento almacenado que cuando le llames como argumentos:

-- Id1:id inicial
-- Id2:id final
-- Actualice el área y el perímetro de los triángulos(utilizando las funciones realizadas) que estén comprendidos entre los id pasados.

  DELIMITER //
    CREATE OR REPLACE PROCEDURE actualizaTriang( IN id1 int, IN id2 int)
      BEGIN
        UPDATE triangulos
          SET 
            area=areaTriang(base, altura),
            perimetro=perimTrian(base,lado2,lado3)
          WHERE id BETWEEN id1 AND id2;
      END //
    DELIMITER ;

  CALL actualizaTriang(2,4);

-- Ejercicio 5. Función área cuadrado.
-- Realizar una función que reciba como argumentos:
-- Lado de un cuadrado
-- Debe devolver el cálculo del área.

DELIMITER //
CREATE OR REPLACE FUNCTION areacuadrado(lado int) 
returns int(20)
BEGIN
  DECLARE resul int;
  SET resul=POW(lado,2);
  RETURN resul;
END //
DELIMITER;

SELECT areacuadrado(4)

-- Ejercicio 6. Función perímetro cuadrado.

-- Realizar una función que reciba como argumentos:
-- Lado de un cuadrado.
-- Debe devolver el cálculo del perímetro.

  DELIMITER //
  CREATE OR REPLACE FUNCTION perimCuadra(lado INT)
  returns int(20)
  BEGIN
    DECLARE resul int;
    SET resul=lado*4;
    RETURN resul;
  END //
  DELIMITER;

SELECT perimCuadra(2)

-- Ejercicio 7. Procedimiento almacenado cuadrados.

-- Realizar un procedimiento almacenado que cuando le llames como argumentos:

--  Id1: Id inicial
--  Id2: Id final

-- Actualice el área y el perímetro de los cuadrados (utilizando las funciones realizadas) que estén comprendidos entre los id pasados.
/* */
    DELIMITER //
    CREATE OR REPLACE PROCEDURE ActualizaCuadrado(IN id1 int, IN id2 int)
      BEGIN
        DECLARE calculoarea int
        UPDATE cuadrados
        SET  calculoarea=areaCuadrado(lado),
          perimetro=perimetroCuadrado(lado)
        WHERE
          ID BETWEEN id1 AND id2;
      END  //
    DELIMITER ;
    CALL ActualizaCuadrado(2,4);

-- Ejercicio 8. Función área rectángulo.

-- Realizar una función que reciba como argumentos:

-- Lado1
-- Lado2

-- Debe devolver el cálculo del área.

DELIMITER //
  CREATE OR REPLACE FUNCTION areaRectang (
  lado1 INT, lado2 int )
  RETURNS int(20)
  BEGIN
  DECLARE resul int;
  SET resul=lado1*lado2;
  RETURN resul;
  END //
DELIMITER;

SELECT areaRectang(4,4);

-- Ejercicio 9. Función perímetro rectángulo.

-- Realizar una función que reciba como argumentos:

-- Lado1
-- Lado2

-- Debe devolver el cálculo del perímetro.

  DELIMITER //
  CREATE OR REPLACE FUNCTION perimRectang (
  lado1 INT, lado2 int )
  RETURNS int(20)
  BEGIN
  DECLARE resul int;
  SET resul=2*(lado1+lado2);
  RETURN resul;
  END //
DELIMITER;

SELECT perimRectang(4,4);

-- Ejercicio 10. Procedimiento almacenado rectángulo.

-- Realizar un procedimiento almacenado que cuando le llames como argumentos:

-- Id1:id inicial
-- Id2:id final

-- Actualice el área y el perímetro de los rectángulos (utilizando las funciones realizadas) que estén comprendidos entre los
-- id pasados.

DELIMITER //
  CREATE OR REPLACE PROCEDURE actualizaRectang(IN id1 INT, IN id2 INT)
    BEGIN
      UPDATE rectangulo 
      SET 
        r.area=areaRectangulo(r.lado1,r.lado2),
        r.perimetro=perimetroRectangulo(r.lado1,r.lado2)
      WHERE
        r.id BETWEEN id1 AND id2;
    END //
  DELIMITER;

SELECT actualizaRectang(4,4);

-- Ejercicio 11. Función área círculo.

-- Realizar una función que reciba como argumentos:

-- radio

-- Debe devolver el cálculo del área

  DELIMITER //
  CREATE OR REPLACE FUNCTION areacirc(radio int) 
  returns float
  BEGIN
    DECLARE resul float;
    SET resul=PI()*POW(radio,2);
    RETURN resul;
  END //
  DELIMITER;

  SELECT areacirc(4);

-- Ejercicio 12. 

-- Función perímetro circulo.

-- Realizar una función que reciba como argumentos:

-- radio

-- Debe devolver el cálculo del perímetro

DELIMITER //
CREATE OR REPLACE FUNCTION perimCirculo(radio int) 
returns float
BEGIN
  DECLARE resul float;
  SET resul=2*PI()*radio;
  RETURN resul;
END //
DELIMITER;

SELECT perimCirculo(4);

-- Ejercicio 13.

-- Procedimiento almacenado círculos.

-- Realizar un procedimiento almacenado que cuando le llames como argumentos:
-- Id1: id inicial
-- Id2: id final
-- Tipo: puede ser a, b o null.
-- Actualice el área y el perímetro de los círculos (utilizando las funciones realizadas) que estén comprendidos entre los id
-- pasados y que sea del tipo pasado. Si el tipo es null cogerá todos los tipos.

DELIMITER //
  CREATE OR REPLACE PROCEDURE actualizaCirculos(IN id1 INT, IN id2 INT, IN t CHAR(1))
    BEGIN
      IF(t IS NULL) THEN
 UPDATE circulo
 SET
 area=areaCirculo(radio),
 perimetro=perimetroCirculo(radio)
 WHERE
 id BETWEEN id1 AND id2;
 ELSE
 UPDATE circulo
 SET
 area=areaCirculo(radio),
 perimetro=perimetroCirculo(radio)
 WHERE
 id BETWEEN id1 AND id2
 AND
 tipo=t;
 END IF;
    END //
  DELIMITER;
 
-- Ejercicio 14. Función media.

-- Realizar una función que reciba como argumentos 4 notas.
-- Debe devolver el cálculo de la nota media.

DELIMITER //
CREATE OR REPLACE FUNCTION notamed (nota1 float, nota2 float, nota3 float, nota4 float)
  RETURNS float
BEGIN
  DECLARE resul float;
  SET resul=(nota1+nota2+nota3+nota4)/4;
  RETURN resul;
END //
DELIMITER;

SELECT notamed(3.5,4.5,5,8.2);

-- Ejercicio 15. Función mínimo.

-- Realizar una función que reciba como argumentos 4 notas.
-- Debe devolver la nota mínima.

DELIMITER //
CREATE OR REPLACE FUNCTION notamin (nota1 float, nota2 float, nota3 float, nota4 float)
  RETURNS float
BEGIN
  DECLARE resul float;
  SET resul=least(nota1,nota2,nota3,nota4);
  RETURN resul;
END //
DELIMITER;

SELECT notamin(3.5,4.5,5,8.2);

-- Ejercicio 16. Función máximo.

-- Realizar una función que reciba como argumentos 4 notas.
-- Debe devolver la nota máxima.

DELIMITER //
CREATE OR REPLACE FUNCTION notamax (nota1 float, nota2 float, nota3 float, nota4 float)
  RETURNS float
BEGIN
  DECLARE resul float;
  SET resul=GREATEST(nota1,nota2,nota3,nota4);
  RETURN resul;
END //
DELIMITER;

SELECT notamax(3.5,4.5,5,8.2);

-- Ejercicio 17. Función moda.

-- Realizar una función que reciba como argumentos 4 notas.

-- Debe devolver la nota que más se repite.

DELIMITER //
CREATE OR REPLACE FUNCTION moda (nota1 float, nota2 float, nota3 float, nota4 float)
  RETURNS float
BEGIN
  DECLARE resul float;
  CREATE OR REPLACE TEMPORARY TABLE tablatemp(
    id int AUTO_INCREMENT PRIMARY KEY,
    valor int
                                             );
INSERT INTO tablatemp(valor) VALUES (nota1), (nota2), (nota3), (nota4);
  SELECT valor INTO resul FROM tablatemp
    GROUP BY valor
    ORDER BY COUNT(*) DESC LIMIT 1;
  RETURN resul;
END //
DELIMITER;

SELECT moda(4,5,5,8);

-- Ejercicio 18. Procedimiento almacenado alumnos.

-- Realizar un procedimiento almacenado que cuando le llames como argumentos:

-- Id1: id inicial

-- Id2: id final

-- Actualice las notas mínima, máxima, media y moda de los alumnos
-- (utilizando las funciones) que estén comprendidos entre los id pasados.

DELIMITER //
  CREATE OR REPLACE PROCEDURE actualizaAlumnos(IN id1 INT, IN id2 INT)
    BEGIN
       UPDATE alumnos
        SET
          notamed=notamed(nota1,nota2,nota3,nota4),
          notamin=notamin(nota1,nota2,nota3,nota4),
          notamax=notamax(nota1,nota2,nota3,nota4),
          moda=moda(nota1,nota2,nota3,nota4)
        WHERE
          id BETWEEN id1 AND id2;
       UPDATE grupos
        SET notamed=(SELECT AVG(resul) FROM alumnos WHERE grupo=1
        AND id BETWEEN id1 AND id2)
      WHERE id=1;

       UPDATE grupos 
        SET notamed=(SELECT AVG(resul) FROM alumnos WHERE grupo=2
       AND id BETWEEN id1 AND id2)
      WHERE id=2;
    END //
  DELIMITER;

-- Ejercicio 19. Procedimiento almacenado conversión.
   
-- Realizar un procedimiento almacenado que le pasas como argumento un id y te 
-- calcula la conversión de unidades de todos los registros que estén detrás de
-- ese id.
-- Si está escrito los cm calculará m, km y pulgadas. Si está escrito en m
-- calculará cm, km y pulgadas y así consecutivamente.


DELIMITER //
  CREATE OR REPLACE PROCEDURE actualizaConvers(IN idInicio INT)
    BEGIN
       UPDATE conversion
        SET
          m=cm/100,
          km=cm/100000,
          pulgadas=cm/0.393701
        WHERE cm IS NOT NULL AND id>idInicio;
        UPDATE conversion
        SET 
          cm=m*100,
          km=m/1000,
          pulgadas=cm/0.393701
        WHERE m IS NOT NULL AND cm IS NULL AND id>idInicio;
        UPDATE conversion
        SET 
          cm=km*100000,
          m=km*1000,
          pulgadas=cm/0.393701
        WHERE km IS NOT NULL AND cm IS NULL AND id>idInicio;
        UPDATE conversion
        set
          cm=pulgadas*2.54,
          m=pulgadas*254,
          km=pulgadas*254000
        WHERE pulgadas IS NOT NULL AND cm IS NULL AND id>idInicio;
    END //
  DELIMITER;